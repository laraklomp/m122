# Übungen zu den Grundlagen

[TOC]

## Übung 1 - Repetition: Navigieren in Verzeichnissen

1. Wechseln Sie mit `cd ` in Ihr Heimverzeichnis
2. Wechseln Sie ins Verzeichnis `/var/log` mit einer absoluten Pfadangabe
3. Wechseln Sie ins Verzeichnis `/etc/udev` mit einer absoluten Pfadangabe
4. Wechseln Sie ins Verzeichnis `/etc/` mit einer relativen Pfadangabe
5. Wechseln Sie ins Verzeichnis `/etc/network/` mit einer relativen Pfadangabe
6. Wechseln Sie ins Verzeichnis `/dev/` mit einer relativen Pfadangabe

## Übung 2 - Wildcards:

Lösen Sie folgende Übungen der Reihe nach (Verwenden Sie soviele Wildcards 
und/oder Braces wie nur irgendwie möglich! ):

1. Erzeugen Sie ein Verzeichnis `Docs` in ihrem Heimverzeichnis
2. Erstellen Sie die Dateien `file1` bis `file10` mit touch im `Docs`-Verzeichnis
3. Löschen Sie alle Dateien, welche einer `1` im Dateinamen haben 
4. Löschen Sie `file2`, `file4`, `file7` mit einem Befehl
5. Löschen Sie alle restlichen Dateien auf einmal 
6. Erzeugen Sie ein Verzeichnis `Ordner` in ihrem Heimverzeichnis
7. Erstellen Sie die Dateien `file1` bis `file10` mit touch im `Ordner`-Verzeichnis
8. Kopieren Sie das Verzeichnis `Ordner` mitsamt Inhalt nach `Ordner2`
9. Kopieren Sie das Verzeichnis `Ordner` mitsamt Inhalt nach `Ordner2/Ordner3`
10. Benennen Sie das Verzeichnis `Ordner` in `Ordner1` um
11. Löschen Sie alle erstellten Verzeichnisse und Dateien wieder

## Übung 3 - Tilde expansions:

Führen Sie jede in der Theorie aufgeführte Erweiterungen der Tilde `~` einmal an Ihrem System aus und stellen Sie sicher, dass Sie deren Funktionsweisen verstanden haben.

## Übung 4 - grep, cut (, awk):

**a)** Erzeugen Sie eine Textdatei mit folgendem Inhalt: 

```
alpha1:1alpha1:alp1ha
beta2:2beta:be2ta
gamma3:3gamma:gam3ma
obelix:belixo:xobeli
asterix:sterixa:xasteri
idefix:defixi:ixidef 
```


Durchsuchen Sie die Datei mit `grep` nach folgenden Mustern (benutzen Sie die
Option `--color=auto`):

- Alle Zeilen, welche `obelix` enthalten
- Alle Zeilen, welche `2` enthalten
- Alle Zeilen, welche ein `e` enthalten
- Alle Zeilen, welche **nicht** `gamma` enthalten
- Alle Zeilen, welche `1`, `2` oder `3` enthalten (benutzen Sie `-E` und eine regex)

**b)**  Gehen Sie von derselben Datei aus wie in Übung a). Benutzen Sie `cut` und
formulieren Sie damit einen Befehl, um nur folgende Begriffe anzuzeigen:

- Alle Begriffe vor dem ersten :-Zeichen
- Alle Begriffe zwischen den beiden :-Zeichen
- Alle Begriffe rechts des letzten :-Zeichen

**c)**  ***Nur für Knobbler***: Gehen Sie wieder von derselben Datei aus wie in Übung a). Benutzen Sie `awk` und
formulieren Sie damit einen Befehl, um nur die Begriffe anzuzeigen, die sich zwischen dem letzten und dem zweitletzten :-Zeichen befinden. Sie kriegen das gleiche Resultat wie bei der zweiten Übung unter **b)**, aber nun dynamisch und die Anzahl Separatoren spielt keine Rolle mehr.

## Übung 5 - Für Fortgeschrittene:

 - Was macht folgender Ausdruck?
		`dmesg | egrep '[0-9]{4}:[0-9]{2}:[0-9a-f]{2}.[0-9]'`

 - Was macht folgender Ausdruck?
		`ifconfig | grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])'`



## Übung 6 - stdout, stdin, stderr:

**a)** Erzeugen Sie eine Textdatei mit folgendem Inhalt:

```
a
b
c
d
e
```

Benutzen Sie zur Erzeugung `<<` indem Sie Zeile fur Zeile an `cat` übergeben, die
Ausgabe wird in eine Datei umgeleitet. Benutzen Sie das Schlusswort `END`.



**b)** Die Ausführung von `ls -z` erzeugt einen Fehler (da es die Option `-z` nicht gibt).
Starten Sie ls mit -z und leiten Sie die Fehler in eine Datei `/root/errorsLs.log` um.

**c)** Erzeugen Sie eine kl. Textdatei und füllen Sie diese mit Inhalt. Geben Sie die
Textdatei mit `cat` aus und leiten Sie die Ausgabe wieder in eine neue Datei um.
Benutzen Sie einmal `>` und einmal `>>` (mehrmals hintereinander). Untersuchen Sie
die beiden Situationen, indem Sie jedesmal den Inhalt der Datei wieder ausgeben.
Was passiert wenn Sie in dieselbe Datei umleiten wollen?

**d)** Leiten Sie die Ausgabe von `whoami` in die Datei `info.txt` um

**e)** Hängen Sie die Ausgabe von `id` an die Datei `info.txt` an

**f)** Leiten Sie die Datei `info.txt` als Eingabe an das Programm `wc` um und zählen
Sie damit die Wörter (`-w`)



