![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

---

### Lernziel:
* Einführung in Linux
* Directories von Linux
* Installation einer Linux Distribution (VM / HW)
* Erste Schritte mit der Linux Distribution

---

![Tux](../x_gitressourcen/tux.png)
(Tux, das Linux Maskotttchen)

## Etwas Geschichte

Linux ist ein Betriebssystem, das ursprünglich von Linus Torvalds als Neugierprojekt begonnen wurde, aber dann ein Eigenleben annahm – inzwischen arbeiten Hunderte von Entwicklern (nicht nur Studenten und Hobby-Programmierer, sondern auch Profis von Firmen wie **IBM**, **Red Hat** oder **Oracle**) an seiner Weiterentwicklung.

Linux wurde inspiriert von Unix, einem in den 1970er Jahren bei den AT&T Bell Laboratories entwickelten Betriebssystem für »kleine« Computer (siehe oben für die Bedeutung von »klein« in diesem Kontext), das sich schnell zum bevorzugten System für Wissenschaft und Technik - und vor allem für Server - entwickelte. **Linux verwendet in sehr weiten Teilen dieselben Konzepte und Grundideen wie Unix**, und für Unix geschriebene Software ist leicht auf Linux zum Laufen zu bringen, aber Linux selbst enthält keinen Unix-Code, ist also ein unabhängiges Projekt.

Im Gegensatz zu Windows und macOS steht hinter Linux keine einzelne Firma, deren wirtschaftlicher Erfolg vom Erfolg von Linux abhängt. **Linux ist »frei verfügbar« und kann von jedem benutzt werden – auch kommerziell –, der die Spielregeln einhält (dazu im nächsten Kapitel mehr)**. Dies zusammen mit dem Umstand, dass Linux inzwischen nicht nur auf PCs läuft, sondern in im Wesentlichen identischer Form vom Telefon (das populärste Betriebssystem für Smartphones, Android, ist ein Ableger von Linux) bis zum größten Großrechner (die 10 schnellsten Rechner der Welt laufen alle unter Linux) auf allen Arten von Rechnern zu finden ist, macht Linux zum flexibelsten Betriebssystem in der Geschichte des modernen Computers.

Strenggenommen ist »Linux« nur der Betriebssystem-Kern, also das Programm, das sich um die Ressourcenverteilung an Anwendungsprogramme und Dienstprogramme kümmert. **Da ein Betriebssystem ohne Anwendungsprogramme aber nicht besonders nützlich ist, installiert man meist eine Linux-Distribution, also ein Paket aus dem eigentlichen »Linux« nebst einer Auswahl von Anwendungs- und Dienstprogrammen, Dokumentation und anderen Nützlichkeiten.** Das Schöne ist, dass die meisten Linux-Distributionen wie Linux selbst »frei verfügbar« und damit kostenlos oder sehr preisgünstig zu haben sind. Auf diese Weise können Sie einen Rechner mit Programmen ausstatten, deren Äquivalente für Windows oder OS X etliche tausend Euro kosten würden, und Sie geraten nicht in Gefahr, wegen Lizenzverstößen belangt zu werden, nur weil Sie Ihre Linux-Distribution auf allen Ihren Computern und denen von Tante Frieda und Ihren Kumpels Susi und Alex installiert haben.

(Quelle "Linux Essentials")

[Linux Distribution Linienplan](./Linux_Linienplan_A3_final.pdf)

[Linux Distribution Timeline](https://upload.wikimedia.org/wikipedia/commons/8/83/Linux_Distribution_Timeline_27_02_21.svg)


---
<br><br><br><br>
---

# Linux Installation

Damit Sie die ersten "Gehversuche" mit Unix oder Linux machen zu können, müssen Sie ein Linux-Subsystem verfügbar machen oder installieren.

Für einmal sind hier Mac-User im Vorteil, denn jedes Mac-System basiert auf Unix, dem grossen Bruder von Linux, und Sie können das auch sofort verwenden.

## Linux unter iOS-Smartphones
Alle iPhones laufen unter einem Apple-eigenen Linux. Unter iPhone können Sie kostenlos aus dem App-Store die App "*iSH*" herunterladen. Damit können Sie einfach Linux-Befehle auf dem Smartphone ausführen, Dateien anlegen usw. Sie werden es aber etwas schwer haben, alle Möglichkeiten auszunutzen und effizient die Befehle einzugeben.

## Linux unter Android-Smartphones
Auch Android ist selber ein Linux-System was von Google für Smartphones weiterentwickelt wurde. Es gibt Apps, mit denen Sie Linux direkt auf dem Smartphone ausführen können.


## Linux unter Windows

Zuerst müssen Sie Ihrem Windows-System bekanntgeben, dass jetzt ein 
Linux-Umgebung installiert werden darf.

Rufen Sie dazu über die Tastenkombination 'WINDOWS' 'R' den Ausführ-Dialog 
auf und geben Sie dort "<mark>**optionalfeatures.exe**</mark>" ein. 
Aktivieren Sie dort "**VM-Plattform**", "**Windows PowerShell 2.0**" 
und "**Windows-Subsystem für Linux**" (WSL) und lassen Sie das System 
nachher neustarten.

![optionalfeatures.jpg](../x_gitressourcen/optionalfeatures.jpg)

Im ('WINDOWS' 'R') **Windows-Store** finden Sie mehr als ein Linux-System zur Installation:

- "Debian"
- "Ubuntu 20.04.4 LTS"
- "Ubuntu 22.04.1 LTS" (empfohlen)
- "openSUSE Leap 15.2"
- "Oracle Linux 8.5"
- "Kali Linux"

_**WICHTIG**: Merken Sie sich das Passwort was Sie vergeben müssen gut, denn es kann Ihnen das niemand zurücksetzen!_


Wenn Sie so WSL, PowerShell und ein Linux-System (wie Ubuntu) installiert haben, werden 
Sie jetzt unter 'WINDOWS' 'R' dann einem der folgenden Wörter ihr neues System 
starten können. Sie haben jetzt folgende neue Subsysteme (==Untersysteme) gleichzeitig
zur Verfügung, mit all denen Sie von jetzt an Systembefehle und eigne Skripte 
erstellen und ausführen lassen können.
- "CMD" (= MS-DOS **C**o**m**man**d**-Line-Tool, bzw. "Eingabeaufforderung")
- "Ubuntu"
- "Powershell"
- "WSL"

![wsl.jpg](../x_gitressourcen/wsl.jpg)

Tipp: 

Lassen Sie mit der rechten Maustaste das Fenster 
"An Taskleiste anheften" damit Sie ab jetzt immer schnellen Zugriff darauf haben.

![taskleiste.jpg](../x_gitressourcen/taskleiste.jpg)


&rarr; Anderes Vorgehen: siehe auch  <https://docs.microsoft.com/de-de/windows/wsl/about>


Oder man installiert eine Bash-Umgebung als separates "Runtime Environment":

- [Cygwin in MobaXterm](https://mobaxterm.mobatek.net/) 
  <br> (Ist etwas komplexer zu installieren, aber funktioniert recht gut:
  <br> [Cygwin Installation Guide](https://geekflare.com/de/cygwin-installation-guide/)
  [(Cygwin Home Page)](https://www.cygwin.com/)

## UNIX bei MacOS 
**Empfohlen:** Das Mac-eigene BSD/UNIX ist schon vorhanden! Tippen Sie dafür im "Spotlight" **TERMINAL** ein. 

Im Detail ist macOS ein proprietäres Betriebssystem, das auf dem Unix-Basisbetriebssystem Darwin aufsetzt, dessen Quelltext unter der freien Apple Public Source License veröffentlicht wird. Die Entwicklung von macOS und Darwin geht auf NeXTStep zurück, das ein Derivat der Berkeley Software Distribution darstellt. Das heutige Darwin ist im Wesentlichen ein Derivat von FreeBSD, ergänzt um OpenBSD-, NetBSD- und eigene Entwicklungen. Ab Version 10.5 bzw. Leopard ist macOS (aber nicht Darwin selbst) als UNIX 03 zertifiziert. (Quelle [Wikipedia](https://de.wikipedia.org/wiki/MacOS))

[(**Erste Schritte in UNIX**)](http://www.netzmafia.de/skripten/unix/unix1.html#1.4.2)


---
<br><br><br><br>
---

## Eine VM installieren
Um eine eigenständige Linux-Systemumgebung zu haben, empfiehlt sich eine "Virtuelle Maschine" (VM) zu installieren und ein Linux Distribution darauf laufen zu lassen. Dies braucht aber etwas mehr System-Ressourcen (Prozessorgeschwindigkeit + Speicherplatz) auf Ihrem Laptop.

- [VirtualBox](https://www.virtualbox.org/manual/ch02.html#installation_windows) von Oracle
- [VM-Ware](https://docs.vmware.com/de/VMware-Tools/11.3.0/com.vmware.vsphere.vmwaretools.doc/GUID-D8892B15-73A5-4FCE-AB7D-56C2C90BD951.html)
- [Hyper-V](https://wiki.ubuntu.com/Hyper-V) 
- macOS: Optional [VirtualBox](https://www.virtualbox.org/manual/ch02.html#installation-mac) [-> Linux auf Mac mit Virtual Box installieren](./Linux auf Mac mit Virtual Box installieren.pdf) , [Parallels](https://www.parallels.com/ch/pd/general/?gclid=CjwKCAjwuYWSBhByEiwAKd_n_mQp0pRwPRbH3yRH_2p5GxeczYw-um9A4GpWwBZwpe2mi2oZyuC9YRoCwm4QAvD_BwE)

### Debian (oder Raspberry Pi Desktop)

Debian ist ein gemeinschaftlich entwickeltes freies Betriebssystem. Debian GNU/Linux basiert auf den grundlegenden Systemwerkzeugen des GNU-Projektes sowie dem Linux-Kernel. Die aktuelle Version ist Debian 11 „Bullseye“, die aktuelle Vorabversion ist Debian 12 „Bookworm“. Debian enthält eine große Auswahl an Anwendungsprogrammen und Werkzeugen; derzeit sind es über 57.000 Programmpakete. Debian ist eine der ältesten, einflussreichsten und am weitesten verbreiteten GNU/Linux-Distributionen.  (Quelle [Wikipedia](https://de.wikipedia.org/wiki/Debian))

[Debian auf Virtual Box](https://wiki.debian.org/VirtualBox)

[**Erste Schritte mit Debian**](https://www.debian.org/doc/manuals/maint-guide/first.de.html)

([Raspberry Pi Os auf Virtual Box](https://webnist.de/wie-man-raspberry-pi-desktop-unter-virtualbox-einrichtet/))


### Ubuntu

Ubuntu, auch Ubuntu Linux, ist eine GNU/Linux-Distribution, die auf Debian basiert. Der Name Ubuntu bedeutet auf Zulu etwa „Menschlichkeit“ und bezeichnet eine afrikanische Philosophie. Die Entwickler verfolgen mit dem System das Ziel, ein einfach zu installierendes und leicht zu bedienendes Betriebssystem mit aufeinander abgestimmter Software zu schaffen. Das Projekt wird vom Software-Hersteller Canonical gesponsert, der vom südafrikanischen Unternehmer Mark Shuttleworth gegründet wurde. (Quelle [Wikipedia](https://de.wikipedia.org/wiki/Ubuntu))

**Windows:**
[auf VirtualBox](https://de.wikihow.com/Ubuntu-in-VirtualBox-installieren)


**MacOS:** <br>
Parallels mit Intel: [Ubuntu auf Parallels](https://www.youtube.com/watch?v=xfMDh1e0pbA) <br> 
UTM mit M1/M2: (ARM-Linux) [Ubuntu auf UTM](https://docs.getutm.app/guides/ubuntu/)


[**Erste Schritte mit Ubuntu**](https://wiki.ubuntuusers.de/Erste_Schritte/)

Hinweis: Um mit SSH auf die Linux-Maschine zugreifen zu können, müssen Sie evtl. den SSH-Server zusätzlich installieren:`sudo apt-get install openssh-server`. Dasselbe gilt für Remote-Desktop.


## Raspberry Pi mit Raspberry Pi OS (Debian) aufsetzen

Die empfohlene Linux-Distribution ist das auf Debian basierende Raspberry Pi OS, früher Raspbian genannt. Dieses Betriebssystem basiert auf der stabilen Version des Debian-Systems der Arm-hard-float-Architektur (armhf) mit Anpassungen für den Befehlssatz des Armv7-Prozessors. Das etwa 3 GB große Image kann auf SD-Karten mit mindestens 4 GB übertragen werden. Nach dem Bootvorgang kann die Partition des Raspberry Pi OS auf die gesamte SD-Karte erweitert werden. Die Raspberry Pi Foundation erstellt auf Basis ihrer Distribution ein eigenes Raspberry-Pi-OS-Image mit passender Firmware für die Raspberry-Pi-Modelle. Daher wird empfohlen, die Distribution immer von der Raspberry Pi Foundation zu beziehen.
(Quelle [Wikipedia](https://de.wikipedia.org/wiki/Raspberry_Pi#Raspberry_Pi_OS))

[Setup Raspberry Pi 3 & 4.pdf](./Setup_Raspberry_Pi3&4.pdf)
Arbeiten Sie Kapitel 1.x, 2.x und 3.x durch. Mit dem **TBZ-Image auf der SD-Karte** (vom Lehrer) können Sie beim Kapitel 1.2 starten ...
 
[**Erste Schritte mit Linux auf dem Raspberry Pi**](https://www.raspberry-pi-geek.de/ausgaben/rpg/2013/05/einfuehrung-linux-auf-dem-raspberry-pi/)




---
<br><br><br><br>
---


# Die Linux Verzeichnishierarchie

Linux kommt, im Gegensatz zu Windows nicht mit verschiedenen Datenträgern (C:/, D:/ usw.) daher, sondern ist in viele Ordner und Unterordner strukturiert. Der Linux-Verzeichnisbaum ist dabei keineswegs willkürlich gewählt, sondern bietet eine durchdachte Struktur. Jedoch sind die Namen der Ordner nicht wirklich sprechend, da sie häufig (in der Softwareentwicklung übliche) Abkürzungen verwenden. Daher wird im Folgenden die Bedeutungen der Hauptverzeichnisse und die wichtigsten Unterverzeichnisse, die es z.B. unter Debian gibt, erläutert.


-   Der Verzeichnisbaum beginnt bei Linux beim Verzeichnis `/`
    (Vergleichbar mit `C:\` unter Windows)

-   Das Verzeichnis `/`wird auch Wurzel- oder Rootverzeichnis genannt

-   Verzeichnisse eine oder mehrere Hierarchiestufen weiter unten werden
    durch `/`-Zeichen getrennt

-   Beispiele:

        /etc/
        /usr/local/nginx
        /usr/bin/
        /home/user1



## Wichtige Verzeichnisse      

		/
Das Wurzelverzeichnis oder Stammverzeichnis genannte Verzeichnis ist die höchste Ebene der Verzeichnisstruktur. In diesem befinden sich alle Verzeichnisstrukturen des Betriebssystems einschließlich weiterer Speichermedien.

		/bin  :: binaries (dt. Binärdateien/Programme)
Die wichtigsten Kommandos (Programme) die von Betriebssystem gebraucht werden wie cp, echo, `mkdir` oder `rm` befinden sich in diesem Verzeichnis. Sie haben alle gemeinsam, dass sie Binärdateien sind und von allen Benutzern ausgeführt werden dürfen. (/sbin nur fürs System/root).


		/dev :: devices (dt. Geräte)
Hier befinden sich Gerätedateien zur Ansteuerung von Hardware wie Festplatten, RAM, Bildschirm oder auch der Maus. Die Dateien stellen die Existenz der Treiber eines Gerätes dar und können auch leere Dateihülsen sein. Andere bieten konkrete Funktionalität an. Für jedes Hardwareteil gibt es vorsorglich einen solchen Eintrittspunkt, auch wenn dieser nicht zwingend benötigt wird.

		/etc ::  et cetera (dt. „alles übrige“), auch edit to configure
Dieser Ordner beinhaltet die meisten systemweit gültigen **Konfigurationsdateien** und stellt damit den zentralen Anlaufpunkt zum Verändern von Einstellungen dar.

		/home     
In diesem Verzeichnis befinden sich standardmäßig die Heimatverzeichnisse der Nutzer. In diesen Ordnern können Benutzer ihre persönlichen Dateien und je nach Anwendung auch benutzerspezifische Konfigurationen ablegen. In Ihrem System ist Ihr Homeverzeichnis zu finden unter `/home/meinusername`.

		~   :: tilde oder "Schlange"
	
		/home/meinusername
Das Tilde-Zeichen `~` kennzeichnet Ihr **Homeverzeichnis** und ist dasselbe wie `/home/meinusername`.

Hier legen Sie Ihre Dateien ab, die Sie bearbeiten. Erste Übungen können Sie auch hier machen.

		/lib :: libraries (dt. Bibliotheken)
Hier sind Funktionsbibliotheken des Systems wieder zu finden. Dies sind Bibliotheken, die bei dem Systemstart benötigt werden und, wenn der Kernel modular aufgebaut ist, die entsprechenden Kernel-Module.

		/opt :: optional
Hier werden alle Programme abgelegt, die nicht im Paketformat vorliegen (folglich händisch installiert und nicht durch Paketmanager verwaltet), wie auch viele kommerzielle Softwarepakete.

		/run    
Dieser Ordner beherbergt Dateien von laufenden Prozessen. In diesem Ordner finden sich die meisten PID-Files (Process identifier).

		/sbin  :: system binaries (dt. Systemprogramme)
In diesem Ordner findet man Programm Binärdateien wieder, die Root-Rechte zum Ausführen benötigen.

		/tmp ::  temporary (temporär)
In diesem Verzeichnis können temporäre Daten abgelegt werden. Zu beachten ist, dass dieses Verzeichnis nach einem Neustart in der Regel bereinigt wird.

		/usr  ::   unix system resources (dt. UNIX Systemressourcen, NICHT USER)
Programme die nicht direkt für das Betriebssytem gebraucht werden, sondern eher für die Benutzer, sind in diesem Ordner zu finden. Dabei enthält dieser Ordner weitere Unterordner, die sich thematisch gliedern.

		/usr/local     
Als wichtiges Unterverzeichnis von /usr ist dieses Verzeichnis zu nennen. In diesem können Benutzer ihre eigenen Programme installieren. Das Verzeichnis ist wie /usr thematisch in Unterordner unterteilt.

		/var  ::   variable (dt. variabel)
In diesem Ordner sind variable Daten wie Logfiles, Mails oder auch Druckerspooler verortet. 

		/var/www/html :: Web-Server Dateien
Dieser Ordner ist nicht standardmäßig in der Debian Distribution angelegt, sollte aber dennoch Erwähnung finden. Er gilt als Standardordner für die Inhalte eines Webservers (z.B. Apache2 für PHP Scripts und Co.).


---

# Checkpoint
* **Herkunft** Linux ist bekannt
* Eine Distribution ist **installiert**. Zugang (**VM**, **SSH**, VNC, VPN) ist erstellt. **Login/PW notiert**.

* **Erste Schritte** sind gemacht (jeweiliger Link , u.a. Durchsicht der Directories). <br>
 *Falls Sie diese nicht direkt nach der Installation gemacht haben, dann wäre es jetzt und [hier](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_003_006.htm#RxxKap00300604004D6F1F04D172) dran!*
