![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

---

### Lernziel:
* Vertiefung der Grundlagen anhand des Projektes
* Praxisnahe Umsetzung von vorgegebenen oder eigenen Ideen
* Automatisierungs-Script mit definierter Konfiguration (oder Parameter oder Menü)
* Robuste Ausführung, d.h. mit Fehlerüberprüfung und entsprechender Fehler-Verarbeitung (Meldung, Abbruch, Wiederholung, ...)
* Ablage und Projektentwicklung auf GitLAB ! [GitLab Installation und Verwendung](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-GitLAB#verwendung-sw-projekt-versionierung)

--- 

# Projektarbeit LB2


Folgendes Systemdesign sollte ihrer Projektidee zugrunde liegen: 


![Systemdesign](./x_gitressourcen/Systemdesign.png)


> Diese Vorlage kann [kopiert](https://miro.com/app/board/uXjVM8D_7t0=/?share_link_id=563348319776) und für die Anforderungsdefintion angepasst werden! [Oder laden Sie die Backup-Datei](./m122-Projekte.rtb)<br>
> **Praxisbezug** siehe Projekt [LB2_Projekt-eBilling-Einzelaufgabe](./LB2_Projekt-eBilling-Einzelaufgabe)


Muss:

- Zentrales **Automationsscript** [`/IhrSystem/automate.sh param1 param2 ...`]
- **Anfrage** auf ein Kundensystem (Server/Dienst): `get: ssh / ftp / request / ...`
- **Verarbeitung** der angeforderten Informationen gemäss Projektanforderung: `data.raw`
- **Weiterreichen** der aufbereiteten Information an weiteres Kundensystem (Server/Dienst): `data.fmt`
- Bei **undefiniertem Zustand oder Datenvorkommen** soll Verarbeitung unterbunden und entsprechend Meldung gemacht werden.

Gewünscht:

- Automation: **CronJob**
- Definierte **Konfiguration**: `ihr_System.cfg`
- Laufende **Protokollierung** der Ausführung: `ihr_System.log`
- **Meldung** an Kundensystem, dass Information abgeholt wurde: `data.zip`
- **Versenden** von Informationen an ein Admin per Mail: `info.mail`



## Zus. Rahmenbedingungen 

* **Zweier- oder Einzelarbeit**. (Bei Zweierarbeit muss jedes Teammitglied eine selbst geschriebene Programmversion abgeben und demonstrieren --> im GitLAB jeweils Branch einrichten. Design und Recherchen der Projektidee darf gemeinsam erfolgen!)
* Aus dem **Internet kopierte Programmteile** sind sauber zu dokumentieren (-> Doku) und Inline zu kommentieren.
* Dokumentation von Design und/oder Implementations-Teilen mit **UML Aktivitätsdiagramm**. (Siehe m319: [UML AD](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N1-UML_Activity_Diagram), [More_AD](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N3-More_AD))
* Abgabe mittels **Demo auf eigenem System** und **Code-Review**.
* **Bewertung**: Siehe von LP abgegebenes [Bewertungsraster](./LB2 Projektarbeit Bewertung V1.1.docx).

## Ernst gemeinte Tipps, die kostbare Zeit (und Nerven) sparen

* Verwenden Sie eine *bereits bestehende* VM mit Linux, bzw. Server.
* Verwenden Sie ein API oder einen *bestehenden* (freien) Server-Dienst.
* Das *Unix des Mac* hat oft andere Features und Sicherheitsbarrieren als Linux!
* Verwnden Sie *keine* Windows-Server (ausser mit Powershell).
* Setzen Sie den Netzwerkadapter der VM auf *Bridge*-Modus.

## Vorgehen Implementation
- **Schrittweise Implementation der einzelnen Features**.
- Jeder Feature-Schritt gleich mit Testdaten (OK/FAIL) **testen und kurz dokumentieren**. (Es soll immer darauf geachtet werden, dass bereits implementierte Features weiterhin korrekt ausgeführt werden, wenn die neu implementierten Features fertiggestellt sind.)
- Jeder Feature-Schritt gleich mit **Inline-Kommentar dokumentieren** gemäss [TBZ-Konvention](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N3-Code_Formatting#quelltext-konvention-tbz-it).
- Spätestens nach jedem getesteten Feature-Schritt eine **Commit-Push-Sequenz auf GitLAB**. (Z.B. mit Tool [GitHUB Desktop](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-GitLAB#verwendung-von-github-desktop))

<br> 

# Meilensteine

|     Meilenstein      | Auftrag | Termin |
|:--------------------:|---|---|
|  MS A <br> Teamnote  | Auftrag von LP *oder* <br> Projekt-Idee in der Anforderungsdefinition formulieren. LP nimmt Anforderungsdefinition ab! <br> GitLAB Link z.H. LP! ("M122-Klasse-THEMA-NAME") | 1. Wo |
| Realisationsphase | Mündliche Statusberichte an LP und regelmässige GitLab Pushs |
| MS B <br> Einzelnote | **Demonstration** des eigenen Automatisierungs-Script auf der persönlichen Systemumgebung (an der TBZ). **Code-Review** des Automatisierungs-Script mit der LP | Gemäss Aufgebot |
| MS C <br> Einzelnote | Abgabe von **Quelltext**, **Projektdoku** inkl. **UML-AD** (siehe oben) auf **GitLAB** | Am Abgabetermin|

# Bewertungsraster

Die Bewertung ihres Projektes kann hier eingesehen werden:
[Bewertungsraster](./LB2 Projektarbeit Bewertung V1.1.docx)

<br> 

---

# Ideen (BASH / Linux):

Folgend Ideen zur **Erhebung von RAW-Daten** vom Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-API):

- Analysedaten: Daten von Analyse-Tool, WEB-APIs, Datengeneratoren, ...
- SQL-Daten: Serverstatus, Berechtigungen, Tabellendaten bestimmter Datenbanken (.sql), ...
- Strukturierte Daten: Dateisystem, Berechtigungen, .cvs, .json, ...
- Verschlüsselte Daten: Transfer der Daten und öffentlichen Schlüsseln, Entschlüsseln, ...
- Status / Performance Daten: Systemdaten, Daten von Perfomance-Tools, Dienstdaten, ...
- IoT-Daten: Sensordaten, MQTT-Kanäle, IoT-WEB-Daten, Cloud-Daten, ...

Folgend Ideen zur **Verarbeitung und Übermittlung von FMT-Daten** an Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-Dienst):

- Execution: Ausführen von weiteren Tools (Install, Clean, ...)
- Config: Setzen von Konfigurationen (System, Tools, Docker, ...)
- Cloud: Transfer in die Cloud (AWS, Azure, ...)
- Publish: Darstellen der Daten (WEB, Print, Statistik-Tool, Graphik-Tool, ...)
- DB Storage: Datenbank SQL insert, ...
- Communication: Teams, Slack, Messenger, WAPP Twitter, Tick Tock, ...


<br> 

---

## Links:

[Tools & Technics](./tools-technics)

[Free API-Webseiten](https://rapidapi.com/collection/list-of-free-apis)

[Free FTP Service](https://www.bplaced.net/)



