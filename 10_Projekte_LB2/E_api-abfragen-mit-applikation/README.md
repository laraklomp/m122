# M122 - Aufgabe

2023-07 MUH

## Applikation mit API-Abfrage


Erstellen Sie ine Applikation bei der ich einen Betrag in CHF
eingeben kann. Und dann will ich eine Umrechnung haben in

- Euro (EUR)
- US-Dollar (USD)
- Ethereum (ETH)
- Bitcoin (BTC)


Benutzen Sie dafür die aktuellen Kurse über eine API.


Zusatz:

Wenn ich nach einiger Zeit den gleichen Betrag wieder
eingebe, sollten Sie darstellen können, was der Betrag
vorher war und was er jetzt ist. 
(Sie müssen die abgefragten Werte speichern um sie dann 
vergleichen zu können. Weiter sollten Sie die Zeit wissen, 
wann das letzte Mal abgefragt wurde und die Differenz
möchte ich auch angezeigt bekommen.)


Hier einige mögliche APIs zum anbinden:

https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis/

https://polygon.io/pricing

<hr>

## Bewertung

| Punkte | Beschreibung | 
|--------|--------------|
|     1  | Eine Ablaufskizze (activity diagram) wird der Lehrperson vorgelegt |
|     1  | Download der aktuellen Kurse |
|     1  | Abfragesystem, Benutzerführung   |
|     1  | Gute und die "schöne" (tabellarische) Darstellung der Daten |
| **4**  | **Total** | 
|     1  | Bonuspunkt für Speicherung der "alten" Daten und Vergleich mit den "neuen" Daten | 
|     1  | Bonuspunkt für  der Vergleiche |
|     1  | Bonuspunkt für Farben in der Darstellung (rot für "runter", grün für "hoch") |
|        |    |
| **1/2** |  Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde  |

<hr>
