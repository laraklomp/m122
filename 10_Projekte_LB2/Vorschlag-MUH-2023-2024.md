# M122 - Abläufe mit einer Scriptsprache automatisieren

[**Modulidentifikation (=Lernziele)**<br> https://www.modulbaukasten.ch/module/122/3/de-DE?title=Abl%C3%A4ufe-mit-einer-Scriptsprache-automatisieren](https://www.modulbaukasten.ch/module/122/3/de-DE?title=Abl%C3%A4ufe-mit-einer-Scriptsprache-automatisieren)


## Zusammensetzung der Modulnote (Leistungsbeurteilung):

- 30% LB1 Bash-Kurs-Prüfung (selber wählbar am 3. oder 4. Modul-Tag )
- 70% LB2 Einzelaufgabe(n) mit vorgegebenen Definitionen.
  <br><br>[**Mögliche Aufgaben**](moegliche-LB2-AufgabenProjekte):
  
	| Projekt | Punkte | Zusatz-<br>Bonus| Alleine-<br>Bonus | Aufgabenstellung |
	|----     |----    |----  |----               |----     |
	| **A**.) |  6     |      |   | [Dateien und Verzeichnisse anlegen](moegliche-LB2-AufgabenProjekte/A_verzeichnisse-und-dateien-anlegen) 
	| **B**.) |  7     |    3 | 1 | [Emailadressen und Brief erstellen](moegliche-LB2-AufgabenProjekte/B_emailadressen-erzeugen) 
	| **C**.) |  6     |    4 | 1 | [Systemleistung abfragen](moegliche-LB2-AufgabenProjekte/C_systemleistung-abfragen) 
	| **D**.) |  5     |    5 | 1 | [APIs-Abfragen mit Datendarstellung](moegliche-LB2-AufgabenProjekte/D_api-abfragen-mit-datendarstellung) 
	| **E**.) |  4     |    4 | 1 | [API abfragen mit Applikation](moegliche-LB2-AufgabenProjekte/E_api-abfragen-mit-applikation) 
	| **F**.) |  8     |      | 1 | [QR-Rechnungen erzeugen lassen](moegliche-LB2-AufgabenProjekte/F_qr-rechnungen-erzeugen)
	| **G**.) | 32     |      | 2 | [eBill von Handwerkerrechnungen](moegliche-LB2-AufgabenProjekte/G_ebill_handwerkerrechnungen) 
	| **H**.) | 5-8    |    + | 1 | [Automatisierte Installation](moegliche-LB2-AufgabenProjekte/H_automatisierte-Installation) |  
	| **I**.) |  8     |      | 1 | [Aktuelles Wertschriften-Depot](moegliche-LB2-AufgabenProjekte/I_aktuelles-wertschriften-depot)
	| **XX**  |  ?     |      | 1 | Weitere/eigene Projekte möglich. Z.B. ein Spiel/Game, Rätsel-Spiel, ...<br>Punkte und Umfang sind mit der Lehrperson abzusprechen. 
        
**Zu beachten:**
```
- Viel ausprobieren! Ohne üben geht gar nichts. Wer viel übt, wird schnell besser!
- Man muss alles mal selber gemacht haben!
- Gemeinsames Arbeiten ist erwünscht. (Jedoch müssen lauffähige Skripts von 
  allen einzeln gezeigt werden.)
- Es sind nur Skript-Sprachen erlaubt. Also kein Java, C#, Kotlin usw.
- Sie brauchen keine grafische Oberfläche und die Maus legen Sie beiseite.
- Alle Variablen, Skripte und Projekte haben "gute" Namen!
- Alle Skripte werden in GitHub, GitLab oder BitBucket eingecheckt.
```

<br>
<br>
<br>
<br>

## Lern-Unterlagen


### a.) Allgemeines

- https://de.wikipedia.org/wiki/Bash_(Shell)
- https://www.selflinux.org/selflinux/html/shellprogrammierung.html

### b.) Vorbereitung für die LB1

**Mit Erklärungen und Übungen** (empfohlen):

- [**&rarr; &rarr; https://gitlab.com/ch-tbz-it/Stud/m122**](https://gitlab.com/ch-tbz-it/Stud/m122) <br>und da beginnen Sie am Besten bitte mit einer **Installation von Linux** <br>für [**Windows**](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/01_Linux_Einf/README.md#linux-unter-windows) oder für [**Mac-OS**](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/01_Linux_Einf/README.md#unix-unter-macos) oder als eine [**Virtulle Machine**](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/01_Linux_Einf/README.md#eine-vm-installieren) 

- installieren von GIT: [&rarr; Eigene GIT-Umgebung (aus Modul M231)](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md)
 
### c.) Referenzen, Nachschlagewerke

- https://openbook.rheinwerk-verlag.de/shell_programmierung (empfohlen)<br>
- https://openbook.rheinwerk-verlag.de/linux
- https://www.gnu.org/software/bash/manual/bash.html
- https://devhints.io/bash
- https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je

 
<br><hr>


## Ablaufplan 2023-Q4

### Klassen <br><mark>AP22a</mark> (MoMo), <br><mark>PE22e</mark> (DoMo), <br><mark>AP22d</mark> (FrMo), <mark>AP22c</mark> (FrNa)

(1. Lehrjahr)

Für die maximale LB2-Note ("6.0"), die 70% der Modulnote ausmacht, 
<br>sind [Projektaufgabe(n)](moegliche-LB2-AufgabenProjekte) im Umfang von <mark>**46** Punkten</mark> nötig.



|Tag |Datum|Thema, Auftrag, Übung |
|----|-----|--------------------- |
|  1 | Mo 15.05. <br> Do 25.05. <br> Fr 26.05. | Einführung und Anleitung <br>**Beginn** mit dem Bash-Selbststudium und Installation einer Linux-Umgebung |
|  2 | Mo 22.05. <br> Do 01.06. <br> Fr 02.06. | **Input** darüber, was an der **Bash-Prüfung** dran kommt.<br> Weiterarbeit mit dem Bash-Selbststudium |
|  3 | Mo 05.06. <br> Do 08.06. <br> Fr 09.06. | Input über **Scheduler/Crontab**.<br> Weiterarbeit mit dem Bash-Selbststudium.<br>Wer will, kann um 10:45 h (15:30 h) **Bash-Prüfung (a)** machen |
|  4 | Mo 12.06. <br> Do 15.06. <br> Fr 16.06. | Input **Projektaufgabe(n)**.<br>Beginn der Projektaufgabe(n) oder Bash-Studium.<br>Um 10:45 h (15:30 h) **Bash-Prüfung (b)** für den Rest |
|  5 | Mo 19.06. <br> Do 22.06. <br> Fr 23.06. | Input über **FTP** und **eMailing**.<br> Weiterarbeit an der Projektaufgabe(n) |
|  6 | Mo 26.06. <br> Do 29.06. <br> Fr 30.06. | Input über **Textformatierung**.<br>Weiterarbeit an der Projektaufgabe(n) |
|  7 | Mo 03.07. <br> Do 06.07. <br> Fr 07.07. | Weiterarbeit an der Projektaufgabe(n) |
|  8 | Mo 10.07. <br> Do 13.07. <br> Fr 14.07. | Weiterarbeit an der Projektaufgabe(n) und <br>ab 09:00 **Projekt-Abgaben** |

<br>

<hr>
&copy; Harald Müller, Mai 2023
