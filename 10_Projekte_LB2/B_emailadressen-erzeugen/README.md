# M122 - Aufgabe

2023-02 MUH


## Emailadressen und Brief erstellen

Files, Listen/Arrays, String-Operationen, Archive, Mailing, FTP, Print
<br>
<br>


**Warnung**:<br>
<mark>Unterschätzen Sie die Aufgabe nicht! Es kann gut sein, dass Sie weit 
über *4 Stunden* Entwicklungs- und Testzeit aufwenden müssen (der Teufel 
liegt im Detail). Grade das Mailen und der FTP-Transfer kann tückisch sein!
Es ist die grosse Menge an Kleinigkeiten, die Sie lösen und beherrschen müssen.
</mark>

<br>
<br>

### Ausgangslage

Sie sind in der internen Informatik der TBZ tätig und 
Sie bekommen für den bevorstehenden Schulanfang eine
[**Liste von Namen (MOCK_DATA.csv)**](https://haraldmueller.ch/schueler/m122_projektunterlagen/b/MOCK_DATA.csv), 
die alle eine neue Emailadressen bekommen sollen.


Zur **Emailadresse** soll auch ein **Initialpasswort** 
generiert werden. Emailadressen und Passwörter
müssen "korrekte" Zeichen enthalten. Also keine
Klammern, Apostrophe, Akzente, Leerzeichen usw. 

E-Mail haben immer nur "kleine" Buchstaben!!!

Aber Vorsicht: Die Inputdaten sind nicht rein, das
ist auch eine Ihrer Aufgaben, dieses Problem zu lösen.


**Beachten Sie**
<br>
Die E-Mailadressen müssen die Form **vorname.nachname@edu.tbz.ch** haben 
und es dürfen keine Buchstaben wegfallen. 

<br>(Also wenn eine **"Adéle Müller"** am Schluss dann eine 
Emailadresse **Adle.Mller@edu.tbz.ch** bekommen würde, ist das 
natürlich falsch. Es muss **adele.mueller@edu.tbz.ch** heissen. 
Sie müssen also die Sonderzeichen (Umlaute äöü, Akzente éàç usw) speziell 
behandeln.)


### Stufe 0


Mit diesem Bash-Befehl (curl = copy from URL) können 
Sie diese MOCK-Datei (MOCK_DATA.csv) direkt vom Server 
abholen und in eine eigene lokale Datei (mock_data.csv) 
speichern:

```curl https://haraldmueller.ch/schueler/m122_projektunterlagen/b/MOCK_DATA.csv > mock_data.csv```

Mittels FTP (file transfer protocol) können Sie die Datei auch abholen.

![ftp-zugangsdaten](../../tools-technics/ftp-zugangsdaten.png)

Weitere Hilfen finden Sie unter [../../tools-technics](../../tools-technics#ftp) 

### Stufe 1

Sie müssen, um das System zu füttern, eine 
Liste aller Emailadressen und das (dazugehörige,
automatisch generierte) Passwörter in einer
Datei namens <br>**YYYY-MM-DD_HH-SS_mailimports.csv**
erstellen lassen.

	[GenerierteEmailadresse1];[GeneriertesPasswort1]
	[GenerierteEmailadresse2];[GeneriertesPasswort2]
	...
	[GenerierteEmailadresse999];[GeneriertesPasswort999]


### Stufe 2

Alle Personen, die jetzt eine neue Emailadresse und Passwort bekommen,
sollen per Papierbrief benachrichtigt werden. Erstellen Sie pro neue
Emailadresse folgende Datei <br>**[GenerierteEmailadresse].brf** 
<br><br>(Die Anschriftadresse passt in ein Fenster-Kuvert, 
die Distanzen für das Einrücken, machen Sie mit Leerzeichen)

	Technische Berufsschule Zürich
	Ausstellungsstrasse 70
	8005 Zürich
	
	Zürich, den [DD.MM.YYYY]
	
	                        [Vorname] [Nachname]
	                        [Strasse] [StrNummer]
	                        [Postleitzahl] [Ort]
									
	
	Liebe [Vorname] (oder - je nach mmännlich oder weiblich, steht in den Daten)
	Lieber [Vorname]
	
	Es freut uns, Sie im neuen Schuljahr begrüssen zu dürfen.
	
	Damit Sie am ersten Tag sich in unsere Systeme einloggen
	können, erhalten Sie hier Ihre neue Emailadresse und Ihr
	Initialpasswort, das Sie beim ersten Login wechseln müssen.
	
	Emailadresse:   [GenerierteEmailadresse]
	Passwort:       [GeneriertesPasswort]
	
	
	Mit freundlichen Grüssen
	
	[IhrVorname] [IhrNachname]
	(TBZ-IT-Service)
	
	
	admin.it@tbz.ch, Abt. IT: +41 44 446 96 60



### Stufe 3 - Archiv-Datei

Erstellen Sie eine "Archiv"-Datei von **allen Dateien**, also
von der Mail- und Passwortliste und auch von allen Briefen. 
Der "Archiv"-Dateiname soll so aussehen:
<br>**YYYY-MM-DD_newMailadr_[IhreKlasse_IhrNachname].zip** (oder .tar, .tar.gz oder .rar je nach Technik)



### Stufe 4 - Mail kommt an

Später werden diese obigen Prozesse für kommende Anwenungen 
vollautomatisch erstellt werden. Dafür erstellen Sie

- 1.) ein CronTab-Eintrag in Linux<br>(oder Aufgabenplanungs-Task bein Windows)
- 2.) ein Mail mit Attachment 

damit Sie wissen, wann die Generierung der Liste und die Briefe
fertig ist und damit die Resultate an den richtigen Ort gelangen.

Das erzeugte Mail brauchen Sie auch, damit Sie die Resultate in einem 
ersten Ausbau mal an sich selber schicken und überprüfen können. 
Später könnten Sie dann das Mail an die **Zielperson** schicken, die 
es dann verarbeiten und verwalten muss.
<br>Gestalten Sie das Mail mit folgendem Text und hängen Sie die Archiv-Datei als
"attachment" an.

Die *Zielperson* kann zu Überprüfungszwecke auch Ihre Lehrperson sein.

*Beachten Sie: Sie haben normalerweise kein Mailserver auf Ihrem Rechner (oder VM).
Sie müssen entweder einen solchen (zusätzlich) installieren oder Sie benutzen eine
"Wegwerf-Mailadresse" über einen Gratis-Provider (z.B. gmx.ch, gmail.com, ...). Ihre
TBZ-Mailadresse können Sie nicht nehmen, da das von der Security her unterbunden wird.*
<br>
<br>[Mail mit PowerShell](../tools-technics/mailing-mit-powershell.jpg)
<br>[Mail mit Python](../tools-technics/mailing-mit-python.jpg)

Subject:

	Neue TBZ-Mailadressen [NumberOfNewMails]

Body:

	Lieber [wählen sie selber eine Anrede je nach Adressat]

	Die Emailadressen-Generierung ist beendet. 
	Es wurden [NumberOfNewMails] erzeugt.

	Bei Fragen kontaktiere bitte [IhreTBZ-Emailadresse]

	Gruss [IhrVorname] [IhrNachname]

Attachment:

	YYYY-MM-DD_HH-SS_newMails_[IhreKlasse_IhrNachname].zip

<br>
<br>
<hr>
<br>

### Stufe 5 - FTP-Transfer

Die Archiv-Datei schicken Sie nun auf einen fremden Rechner mittels **FTP**. (Von der 
sicheren Datenübertragung wie FTPs sehen wir hier mal ab damit Sie nicht zusätzlich 
ausgebremst werden).
Benutzen Sie bitte folgende Zugangsdaten:


Browserzugang zum Testen oder Nachschauen:

[**FTP-Zugangsdaten**](../../tools-technics/ftp-zugangsdaten.md)


### Stufe 6 - Vollautomatische Verarbeitung

Sie binden Ihr Skript in den Scheduler (CronTab, Aufgabenplaner) ein. Die 
Input-Daten bekommen Sie vom gleichen Ort wie in Stufe 5. 
Sie müssen sie via FTP abholen.


### Stufe 7 - Dublettenkontrolle

Finden Sie heraus, was man machen muss, wenn es in der Liste zwei Personen
hat, die den gleichen Vornamen und Nachnamen haben (solls ja geben).
Wie teilen Sie wem welche Emailadresse zu?




### Abgabe-Bedingung

Erweitert:

Der Abgabezeitraum gibt die Lehrperson bekannt und 
ist etwa **30 Minuten** lang. In dieser Zeit muss 
ihr System **über ein Start-Skript** laufen.
Eingriffe "von Hand" sind nicht erlaubt. <mark>Es zählt, was Ihr
System zum Abgabe- und Testzeitpunkt leistet</mark>. Für diesen
**Abschluss-Test** wird von Ihrer Lehrperson eine 
**separates Test-Datei** in der gleichen Form bereitgestellt, die dann 
korrekt verarbeitet werden soll.


## Bewertung

| Punkte | Beschreibung |
|--------|--------------|
|  3 | Datei mit Mailadr./Passw. liegt vor, Mails haben korrekte Form, nur Kleinbuchstaben und nur einfache Buchstaben |
|  1 | Alle Briefe korrekt erstellt              |
|  1 | Archiv-Datei ist erstellt (tar, gz, zip)  |
|  2 | Korrektes Mail inkl. Attachment kommt an  |
|**7**|  **Total** |                            |
|  3 | Bonus: Dublettenkontrolle inkl. durchgeführtem Test |
|||
| **1/2** | Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde |
|||
