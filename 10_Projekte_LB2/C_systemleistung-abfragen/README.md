# M122 - Aufgabe

2023-03 MUH


## System-Leistung abfragen

Für eine automatische Systemüberwachung von Servern 
und/oder auch Clients sollen Sie "regelmässig" (cron)
eine Serie von Leistungs-Daten ausgeben.

### Aufgabenstellung

Verwenden dafür Bash- Shell-Scripting oder auch PowerShell
und stellen Sie sicher, dass es auf Ihrem System 
ausgeführt werden kann. 


**1.) Formattierte Inhalte**

Formattieren Sie alles in eine gut leserliche **Tabellen-Form**.

Folgendes ist auszugeben:

- 1. Der Hostname des Systems
- 2. Aktuelle IP-Adresse 
- 3. Die Betriebssystemversion
- 4. Der Modellname der CPU
- 5. Die Anzahl der CPU-Cores
- 6. Der gesamte und der genutze Arbeitsspeicher
- 7. Die Grösse des verfügbaren Speichers
- 8. Die Grösse des freien Speichers
- 9. Die Gesamtgrösse des Dateisystems
- 10. Die Grösse des belegten Speichers auf dem Dateisystem
- 11. Die Grösse des freien Speichers auf dem Dateisystem
- 12. Die aktuelle Systemlaufzeit
- 13. Die aktuelle Systemzeit
- 14. Trenner für die nächste Ausgabe

**2.) Dateiausgabe wahlweise** (mit einem "switch" `-f`)

Gefordert ist die Ausgabe **wahlweise** direkt auf das 
<br>Terminal, bzw. die Console, wie auch in eine Datei.

- Wenn man **keine Option** angibt: Nur die Terminal-Ausgabe ohne Datei.
- Wenn man den **"Switch"** (die Option) `-f` angibt, soll zusätzlich die 
<br>Datei **[YYYY-MM]-sys-[hostname].log** erzeugt werden.
<br>(Immer in die gleiche Datei schreiben. Das nennt man ein **"Log"**)

Tipp: Benutzen Sie für den Timestamp `date '+%Y-%m-%d_%H%M'` bzw. `date '+%Y-%m'`
und für den Hostnamen `hostname` oder `uname -n` und den Befehl `df ` für Disk-Angaben.


**3.) Regelmässigkeit**

Binden Sie Ihr Skript in die `crontab` ein
und wählen Sie einen geeigneten Ausführungs-Takt.

Tipp: Prüfen Sie, ob Ihr **cron** eingeschaltenist mit folgendem Befehl:

	service cron status

Falls cron nicht läuft, können Sie es damit einschalten:

	sudo service cron start


 
### Resultat

Ihr Resultat könnte so in dieser Art aussehen (schöner ist besser):

Tipp: Benutzen Sie den `printf`-Befehl


| Text | Wert | 
|------|------|
| free disk space | 80 GB |
| free memory  | 07 GB | 
| ...  | ... | 
| ...  | ... | 
| ...  | ... | 


<hr>

## Bewertung

| Punkte | Beschreibung | 
|-------|--------------|
|     3 | Alle oben genannten Sytem-Infos   |
|     1 | Ausgabe in die Datei mit "Switch" und richtigem Dateiname | 
|     1 | Regelmässige Ausführung (Abgabe von 3 Log-Einträgen, die das System im Takt erstellt hat)   |
|     1 | "Gute/schöne" Tabellen-Darstellung   |
| **6** | **Total** | 
|       |    |
|       | Erweiterungsmöglichkeiten   |
|     2 | Verschicken einer E-Mail wenn ein "Schwellwert" überschritten wird |
|     2 | Darstellung auf einer Webseite (HTML ->  als index.html in Ihr Verzeichnis, siehe Zugangsdaten ) für das Monitoring mit den Werten und zusätzlich einer Ampel-Darstellung grün/gelb/rot [**FTP-Zugangsdaten**](../../tools-technics/ftp-zugangsdaten.md)|
|||
| **1/2** | Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde |
|||


