# M122 - Aufgabe

2023-05 MUH

## APIs-Abfragen mit Datendarstellung

### Aufgabenstellung

Für eine automatische Abholung von frei verfügbaren
Informationen kann man APIs (application programming interfaces) 
benützen. Erstellen Sie "regelmässig" (muss in cron eingebunden sein)
eine Serie von für Sie wertvolle und tagesaktuelle Informationen.

Verwenden Sie dafür Bash-Shell-Script (ausnahmsweise auch PowerShell oder Python)

Die Informationen sollen (lokal oder auf einer Domain) als HTML-source "schön" 
und so dargestellt werden, dass sie einfach mit dem Browser angesehen werden können.

Besser ist es, wenn die Informationen im Internet abrufbar sind 
[**FTP-Zugangsdaten**](../../tools-technics/ftp-zugangsdaten.md)

Lassen Sie sich die Informationen auch mailen.

Binden Sie Ihr Skript in die `crontab` ein (sudo crontab -e)
und wählen Sie einen geeigneten und vernünftigen Ausführungs-Takt.

Mögliche Informationen könnten sein:
- Hauptwährung-Umrechnungskurse (USD, EUR zu CHF)
- Kurse von Crypto-Währungen
- Aktienkurse oder Index-Entwicklungen
- Wetterdaten (nicht nur von einem Ort und über mehrere Zeitpunkte)
- Wassertemperaturen
- Sportresultate
- Kultur- und Event-Termine
- Aktuelle Preise von Gebrauchs- oder Konsumgüter

Sie können auch APIs von Einmaldaten abrufen wie
- Distanzen von Orten
- Bahnverbindungen
- Flugverbindungen
- Himmels-/Astronomiedaten


Hier einige mögliche APIs zum anbinden:

https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis/

https://polygon.io/pricing

<hr>

## Bewertung

| Punkte | Beschreibung | 
|--------|--------------|
|     1  | Eine Ablaufskizze (activity diagram) wird der Lehrperson vorgelegt |
|     1  | Für die Anbindung der ersten API mit einer Informationsklasse  (Wetter, Kurse, ...)  |
|     1  | Für die Verarbeitung von JSON-Files usw. (einfache Auflistung aller Daten)  |
|     1  | Einbindung in crontab für die regelmässige Verarbeitung  |
|     1  | "Schöne" (tabellarische) Darstellung der Informationsklasse(n)  |
| **5**  | **Total** | 
|     1  | Bonuspunkt für jede Anbindung einer weiteren API, bzw. Informationsklasse | 
|     2  | Bonuspunkte für eine applikatorische Bearbeitung (nur ausgewählte Daten mit Steuerung über eine andere Config-Datei) |
|     1  | Bonuspunkt für die Onlinebereitstellung (FTP, HTML) mindestens einer Informationsklasse  |
|     1  | Bonuspunkt für die regelmässige Zusendung per Mail (an sich selber)  |
|        |   |
| **1/2**  |  Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde  |
|        |   |

<hr>
