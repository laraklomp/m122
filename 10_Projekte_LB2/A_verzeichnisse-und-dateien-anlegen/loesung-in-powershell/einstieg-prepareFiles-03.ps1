# mal schauen, ob ob ich aus Verzeichnissen, Dateien lesen kann

$verzeichnis_namensdateien = "_namensdateien"

# Da PowerShell objektorientiert ist, kann man gleich das 
# Name-Attribut aus der Resultatsliste erfragen.
# Dies wird sogleich in eine Variable gespeichert.
$allFiles = (dir $verzeichnis_namensdateien).name 
echo $allFiles

# Skript anhalten, um zu sehen, obs Fehler gab
pause