﻿            #  man kann auch eine Liste von Adresssaten angeben a@xy.com, b@xy.com
$to         = "harald.mueller@bluewin.ch"
$cc         = ""
$bcc        = ""

$subject    = "Email Subject"
$body       =  "Hallo "+$To.Split("@")[0]
$body      += "`n"
$body      += "`n`nSchön, dass das klappt mit dem Mail!"
$body      += "`n"
$body      += "`nMit freundlichen Grüssen"
$body      += "`nHarald Müller"


$SMTPServer = "mta.tam.ch" # Mailserver der Berufsschulen des Kantons Zürich
$SMTPPort   = "587"        # SSL-Port ist 587, Normal-Port wäre 465, man müsste dann aber "$SMTP_objekt.EnableSSL = $false" setzen
            #  vorname.nachname@edu.tbz.ch
$Username   = "harald.mueller@tbz.ch" 
$Password   = "" # Hier Passwort eingeben


$Message_Objekt    = New-Object System.Net.Mail.MailMessage
$Message_Objekt.from = $username
$Message_Objekt.subject = $subject
$Message_Objekt.body = $body
$Message_Objekt.to.add($to)
if ($cc.Length -gt 5) {         # falls ein cc angegeben wurde
    $Message_Objekt.cc.add($cc) 
}
if ($bcc.Length -gt 5) {        # falls ein bcc angegeben wurde
    $Message_Objekt.Bcc.add($bcc)
}

$Message_Objekt.Attachments.add("C:\Users\harald\OneDrive - TBZ\M122-AutomMitSkriptenPS\05_Konkrete_Aufgabenstellungen\auto-mailer\mailBeilage1.pdf")

# es würde auch folgendes gehen
#[array]$attachmentArr = @()
#$attachmentArr = "mailBeilage1.pdf", "mailBeilage2.pdf"

#foreach ($a in $attachmentArr) {   # falls mehr als eine Datei angegeben wurde wird jedes addiert
#    $Message_Objekt.Attachments.add($a)
#}

$SMTP_objekt = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
$SMTP_objekt.EnableSSL = $true  # $False  setzen, wenn man den Normal-Port nimmt
$SMTP_objekt.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);
$SMTP_objekt.send($Message_Objekt)
write-host "Mail Sent"