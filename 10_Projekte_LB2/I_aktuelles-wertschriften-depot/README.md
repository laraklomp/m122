# M122 - Aufgabe

2023-08 MUH

## Aktuelles Wertschriften-Depot


Machen Sie ein (fiktives) Aktien-, Fremdwährungs- 
und Krypto-Depot mit aktuellen Daten, die Sie aus 
Online-APIs abholen sollen.

https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis
 
Also es soll jemand z.B. 10 Aktien von Novartis und z.B.
3000 USD und z.B. 0.1 Bitcoins in einem Depot halten. 

Es wurde damals X Franken für die Novartis-Aktien bezahlt 
und Y Franken für die USD und Z Franken für Bitcoins bezahlt. 

Die Frage ist nun: 

- Was ist dieses Depot heute in CHF wert?
- Diese Werte-Entwicklung möchte ich gerne über die Zeit (regelmässig) verfolgen, also den Wert alle x Stunden oder Tage


<hr>

## Bewertung

| Punkte |Beschreibung | 
|--------|------------ |
|    3   | Mehrere aktuelle Kursdaten sind heruntergeladen und mit den Assets verrechnet  |
|    2   | Depot-Wert wird ermittelt historisch & aktuell |
|    3   | Die Depotwerte haben eine Zeitschreibung       |
|  **8** | **Total** |
|        |           |
| **1/2**| Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde |
|        |   |
