# M122 - Aufgabe

2023-07 MUH


## ebill von Handwerkerrechnungen


### Ausgangslage ("Use Case")

Sie arbeiten in einer Firma, die sich zum Ziel gesetzt hat, 
für ihre Kunden und auch um neue Kunden zu gewinnen, die 
Rechnungen, die die Handwerker für deren Kunden erstelle,
den elektronischen Zahlungsweg anzubieten.

Der elektronische Zahlungsweg wird nicht über das Verschicken
einer PDF-Rechnung gemacht, wo der Kunde die Rechnung über das
Eintippen der Zahlungsdaten im eBanking machen muss, sondern als
 [**ebill**](https://ebill.ch). Also als eine Rechnung, die dem Kunden
des Handwerkers direkt in sein eBanking hineingeschickt wird und 
von dort aus direkt mit zwei Klicks bezahlt werden kann.
 
**Resultat**

![x-ressourcen/example-qr-bill-de.six-image.original.1020.png](x-ressourcen/example-qr-bill-de.six-image.original.1020.png)

<hr>

### Aufgabenstellung

![ebill-uebersicht.jpg](x-ressourcen/ebill-uebersicht.jpg)


Sie bekommen auf dem System des Rechnungsstellers in einem 
eigenen Format direkt von den Kassensystemen Rechnungsdateien 
mit allen Informationen, die es für die Rechnungsstellung 
über e-Bill braucht. 

Sie holen diese per FTP ab und erstellen daraus 
- Zwischenresultat a.) Eine menschenlesbare Rechnung im Klartext (Zur Vereinfachung nur als Text-Format, produktiv wäre das ein PDF)
- Zwischenresultat b.) Eine maschinenlesbare Rechnung im XML-Format für das SIX-PaymentServices-System (Schweizer Marktleader für die Zahlungsabwicklung)

Diese beiden Dateien geben Sie per FTP in das «Zahlungssystem» ab.
Dann erfolgt, für Sie im Hintergrund die Weiterleitung 
ins Bankensystem. Ob das geklappt hat, wissen Sie erst, 
in dem das Zahlungssystem eine Quittungsdatei produziert 
hat und Ihnen zur Verfügung stellt.

Ihrem Kunden (dem Rechnungssteller =Biller) stellen Sie 
diese Quittungsdatei sowohl per E-Mail wie auch im 
Rechnungsstellungs-System in einem Archiv (.zip) zur 
Verfügung, damit er überprüfen kann, ob die Rechnung 
korrekt verschickt wurde.

**Biller-System**

Sie bekommen vom Biller-Sytem **mehrere** solche 
Dateien


Dateiname:	rechnung24018.data

	Rechnung_24018;Auftrag_A003;Zürich;21.03.2024;10:22:54;ZahlungszielInTagen_30
	Herkunft;41010000001234567;CH7189144353227895511;K821;Adam Adler;Bahnhofstrasse 1;8000 Zuerich;CHE-111.222.333 MWST;harald.mueller@tbz.ch
	Endkunde;41301000000012497;CH1889144876152963546;Autoleasing AG;Aareweg 100;5000 Aarau
	RechnPos;1;Einrichten E-Mailclients;6;25.00;150.00;MWST_0.00%
	RechnPos;2;Konfig & Schulung Scanningcenter;1;1200.00;1200.00;MWST_0.00%


Weitere Daten:

- [rechnung24016.data](x-ressourcen/rechnung24016.data)
- [rechnung24017.data](x-ressourcen/rechnung24017.data)
- [rechnung24018.data](x-ressourcen/rechnung24018.data)
- [rechnung24019.data](x-ressourcen/rechnung24019.data)


**Zu beachten:** Es klingt trivial. 
Es können 1 bis n Rechnungspositionen 'RechnPos' erscheinen! Wenn aber 
keine Rechnungspositionen vorhanden sind, ist es keine Rechnung. 
Und weil bei so einer automatischen  Verarbeitung kein User hier 
sitzt, muss man das Problem mitteilen (zumindest im Log-File) und 
die Verarbeitung für 'diese aktuelle' Rechnung abbrechen. 
<br>&rarr; Man muss also eine Eingangsprüfung machen.

Sie müssen aus **jeder Rechnungsdatei** (.data) **zwei Dateien** erstellen
(ein TXT-File und ein XML-File) und diese müssen einen bestimmten Namen haben:
<br>**[Kundennummer]_[Rechnungsnummer]_invoice.xml** (also konkret ‘K821_24018_invoice.xml’)
<br>**[Kundennummer]_[Rechnungsnummer]_invoice.txt** (also konkret ‘K821_24018_invoice.txt’)

Die Dateinamen werden aus den **Inhalten** der .data-Datei gebildet (generiert): 
<br>(aus **Kundennummer** "K821" und aus der **Rechnungsnummer** "24018")

<br>Dateiname Zwischenresultat a.):	K821_24018_invoice.txt
<br>Dateiname Zwischenresultat b.):	K821_24018_invoice.xml


### Zwischenresultat a.) Rechnung in menschenlesbaren Klartext

Eine solche Datei wird den Endkundenen mitgeschickt, damit
überprüft werden kann, was zu bezahlen ist. Im eBanking-System 
haben Endkunden die Möglichkeit, die Rechnung zurückzuweisen 
und somit nicht zu bezahlen. 


**Inhalt von K821_24018_invoice.txt:**

	-------------------------------------------------
	
	
	
	Adam Adler
	Bahnhofstrasse 1
	8000 Zuerich
	
	CHE-111.222.333 MWST
	
	
	
	
	Uster, den 21.03.2024                           Autoleasing AG
	                                                Aareweg 100
	                                                5000 Aarau
	
	Kundennummer:      K821
	Auftragsnummer:    A003
		
	Rechnung Nr       24018
	-----------------------
	1   Einrichten E-Mailclients              5      25.00  CHF      125.00
	2   Konfig & Schulung Scanningcenter      1    1200.00  CHF     1200.00
                                                                 -----------       
                                                      Total CHF     1325.00

                                                      MWST  CHF        0.00
	
	
	
	
	
	
	
	
	Zahlungsziel ohne Abzug 30 Tage (20.04.2024)
	
	
	Empfangsschein             Zahlteil                
	
	Adam Adler                 ------------------------  Adam Adler       
	Bahnhofstrasse 1           |  QR-CODE             |  Bahnhofstrasse 1 
	8000 Zuerich               |                      |  8000 Zuerich     
	                           |                      |  
	                           |                      |  
	CH18 8914 4876 1529 6354 6 |                      |  CH18 8914 4876 1529 6354 6
	                           |                      |  
	Autoleasing AG             |                      |  Autoleasing AG
	Gewerbestrasse 100         |                      |  Gewerbestrasse 100
	5000 Aarau                 |                      |  5000 Aarau
	                           ------------------------
	Währung  Betrag            Währung  Betrag
	CHF      1325.00           CHF      1325.00								

	-------------------------------------------------


#### Vereinfachungen zum produktiven Einsatz

- 1.) Normalerweise sind diese Rechnungen hier in PDF erstellt. 
Damit es weniger Aufwand gibt, wird hier darauf verzichtet, aus 
dem Text ein PDF zu erzeugen. Die Einrückungen bräuchten eigentlich 
Tabulatoren. Das kann man mit dem "reinen Textformat" nicht machen. 
Stattdessen wird mit Leerzeichen eingerückt (vorne auffüllen).

- 2.) Die Erzeugung des QR-Codes im Einzahlungsschein-Bereich 
erfordert den Aufruf einer API-Funktion, die dann eine Grafik erzeugt 
und die dann in ein PDF integriert werden müsste. Auch darauf wird verzichtet. 
Stattdessen zeichnen wir einfach einen Rahmen mit Strich-Zeichen und 
schreiben das Wort "QR-CODE" hinein. 

- 3.) Auch auf die Auszeichnung der Texte wird verzichtet 
(verschiedene Schriftgrössen, Fettschrift, Titelschrift, Logos).

- 4.) Es gibt Anwendungen, bei denen Rechnungen im B2B-Bereich
(Business-to-Business), also unter Firmen direkt verschickt werden. 
Dort könnte das Aufzählen von Rechnungspositionen (der Artikel) samt
Mehrwertsteuersätzen und Rabatten in der .xml-Datei nötig werden. 
Aber für "eBill" bei SIX sind wir jedoch im Bereich B2C 
(Business-to-Customer) und da ist das detaillierte Aufzählen nicht 
verlangt. Wir können darauf auch verzichten und wir machen nur
die summarische Angabe.

-5.) Auf das aufsummieren der verschiedenen Mehrwertsteuersätze
wird verzichtet. In diesen Beispielen hier können Sie davon ausgehen, 
dass es keine MWSt gibt.



### Zwischenresultat b.) Rechnung im (maschinenlesbaren) XML-Format

**Inhalt von K821_24018_invoice.xml:**

	-------------------------------------------------

	<XML-FSCM-INVOICE-2003A>
	<INTERCHANGE>
		<IC-SENDER>
		<Pid>???? Biller-Party-ID ????</Pid>
		</IC-SENDER>
		<IC-RECEIVER>
		<Pid>???? Payer-Party-ID ????</Pid>
		</IC-RECEIVER>
		<IR-Ref />
	</INTERCHANGE>
	<INVOICE>
		<HEADER>
		<FUNCTION-FLAGS>
			<Confirmation-Flag />
			<Canellation-Flag />
		</FUNCTION-FLAGS>
		<MESSAGE-REFERENCE>
			<REFERENCE-DATE>
			<Reference-No>????? Timestamp now ?????</Reference-No>
			<Date>YYYYMMDD (=heute)</Date>
			</REFERENCE-DATE>
		</MESSAGE-REFERENCE>
		<PRINT-DATE>
			<Date>YYYYMMDD (=heute)</Date>
		</PRINT-DATE>
		<REFERENCE>
			<INVOICE-REFERENCE>
			<REFERENCE-DATE>
				<Reference-No>???? RECHNUNGSNUMMER ????</Reference-No>
				<Date>YYYYMMDD (=Rechnungsdatum)</Date>
			</REFERENCE-DATE>
			</INVOICE-REFERENCE>
			<ORDER>
			<REFERENCE-DATE>
				<Reference-No>A003 (=Auftrags-Nummer)</Reference-No>
				<Date>YYYYMMDD (=Heute)</Date>
			</REFERENCE-DATE>
			</ORDER>
			<REMINDER Which="MAH">
			<REFERENCE-DATE>
				<Reference-No></Reference-No>
				<Date></Date>
			</REFERENCE-DATE>
			</REMINDER>
			<OTHER-REFERENCE Type="ADE">
			<REFERENCE-DATE>
				<Reference-No>???? Timestamp now ????</Reference-No>
				<Date>YYYYMMDD (=heute)</Date>
			</REFERENCE-DATE>
			</OTHER-REFERENCE>
		</REFERENCE>
		<BILLER>
			<Tax-No>ZZZZZZZZZZZ Mehrwertsteuernummer des Absenders</Tax-No>
			<Doc-Reference Type="ESR-ALT "></Doc-Reference>
			<PARTY-ID>
			<Pid>ZZZZ Biller Party-ID ZZZZZ</Pid>
			</PARTY-ID>
			<NAME-ADDRESS Format="COM">
			<NAME>
				<Line-35>XXXXXXXXXXXXXXXXXXXX</Line-35>
				<Line-35>XX Biller Adresse XX</Line-35>
				<Line-35>XXXXXXXXXXXXXXXXXXXX</Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</NAME>
			<STREET>
				<Line-35></Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</STREET>
			<City></City>
			<State></State>
			<Zip></Zip>
			<Country></Country>
			</NAME-ADDRESS>
			<BANK-INFO>
			<Acct-No></Acct-No>
			<Acct-Name></Acct-Name>
			<BankId Type="BCNr-nat" Country="CH">001996</BankId>
			</BANK-INFO>
		</BILLER>
		<PAYER>
			<PARTY-ID>
			<Pid>???? Payer-Party-ID ????</Pid>
			</PARTY-ID>
			<NAME-ADDRESS Format="COM">
			<NAME>
				<Line-35>YYYYYYYYYYYYYYYYYYY</Line-35>
				<Line-35>YY Payer-Adresse YY</Line-35>
				<Line-35>YYYYYYYYYYYYYYYYYY</Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</NAME>
			<STREET>
				<Line-35></Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</STREET>
			<City></City>
			<State></State>
			<Zip></Zip>
			<Country></Country>
			</NAME-ADDRESS>
		</PAYER>
		</HEADER>
		<LINE-ITEM />
		<SUMMARY>
		<INVOICE-AMOUNT>
			<Amount>#### mit führenden Nullen, in Rappen (ohne Punkt) ########</Amount>
		</INVOICE-AMOUNT>
		<VAT-AMOUNT>
			<Amount></Amount>
		</VAT-AMOUNT>
		<DEPOSIT-AMOUNT>
			<Amount></Amount>
			<REFERENCE-DATE>
				<Reference-No></Reference-No>
				<Date></Date>
			</REFERENCE-DATE>
		</DEPOSIT-AMOUNT>
		<EXTENDED-AMOUNT Type="79">
			<Amount></Amount>
		</EXTENDED-AMOUNT>
		<TAX>
			<TAX-BASIS>
			<Amount></Amount>
			</TAX-BASIS>
			<Rate Categorie="S">0</Rate>
			<Amount></Amount>
		</TAX>
		<PAYMENT-TERMS>
			<BASIC Payment-Type="ESR" Terms-Type="1">
			<TERMS>
				<Payment-Period Type="M" On-Or-After="1" Reference-Day="31">XX</Payment-Period>
				<Date>YYYYMMDD (=Zahlungsziel-Datum)</Date>
			</TERMS>
			</BASIC>
			<DISCOUNT Terms-Type="22">
			<Discount-Percentage>0.0</Discount-Percentage>
			<TERMS>
				<Payment-Period Type="M" On-Or-After="1" Reference-Day="31"></Payment-Period>
				<Date></Date>
			</TERMS>
			<Back-Pack-Container Encode="Base64"> </Back-Pack-Container>
			</DISCOUNT>
		</PAYMENT-TERMS>
		</SUMMARY>
	</INVOICE>
	</XML-FSCM-INVOICE-2003A>
	
	-------------------------------------------------

<br>

### System-Zugänge

**Browser:** https://haraldmueller.ch/schoolerinvoices (Passwort: "tbz")
<br>https://haraldmueller.ch/schoolerinvoices/out (Passwort auf html-Seite: "tbz")
<br>https://haraldmueller.ch/schoolerinvoices/in 

**FTP-Zugang:**

Als Hilfe zum Überprüfen, was Sie mit dem Skript schicken oder empfangen, 
verwenden Sie am Besten einen FTP-Client wie "FileZilla". Speichern Sie dort 
im "Servermanager" die Einstellungen.
![FileZillaClient](x-ressourcen/filezillaclient.jpg)


	HOST: "ftp.haraldmueller.ch"
	USER: "schoolerinvoices"
	PASS: "Berufsschule8005!"
	PATH: "/in/[Klasse]/[IhrNachname]"
	PATH: "/out/[Klasse]/[IhrNachname]"


![ebill-uebersicht-details.jpg](x-ressourcen/ebill-uebersicht-details.jpg)


**Browser:** https://coinditorei.com/zahlungssystem (Passwort: "tbz")
<br>https://coinditorei.com/zahlungssystem/in
<br>https://coinditorei.com/zahlungssystem/out 
<br>
<br>Zur Abgabe der Rechnung als TXT und XML auf dem Abgabeserver (Zahlungssystem/in/[KlasseUndIhrNachname]) per FTP

**FTP-Zugang:**

	HOST: "ftp.coinditorei.com"
	USER: "zahlungssystem"
	PASS: "Berufsschule8005!"
	PATH: "/in/[KlasseUndIhrNachname]"
	PATH: "/out/[KlasseUndIhrNachname]"

<br>
<br>
<br>
<br>
<br>Wenn Sie die Dateien `.._invoice.txt` und `.._invoice.mxl` abgeschickt haben,
<br>klicken Sie auf das Zahnradsymbol auf dem Zahlungssystem-Rechner.
<br>
<br>Der erzeugt dann das `Quittungs-File`. 
<br>
<br>Danach kann das Programm,  
<br>im folgenden Takt (oder ein separates Programm) den 
<br>zweiten Teil der Arbeit machen (das Zippen und das Mailen) und
<br>dem User (Biller) zurückschicken der bestätigten Zahlung
<br>
<br>[https://coinditorei.com/zahlungssystem](https://coinditorei.com/zahlungssystem)
<br>![zahnrad.png](x-ressourcen/zahnrad.png)
<br>[https://coinditorei.com/zahlungssystem](https://coinditorei.com/zahlungssystem)
<br>
<br>
<br>
<br>


**EMail:**

Sie verschicken dem Ersteller der Rechnung (EMail-Adresse steht in den 
Daten drin) ein Mail mit Text und Attachment. Das Attachment ist das 
ZIP-File (oder tar-File), was sich aus der Rechnung in XML-Form und in 
TXT-Form sowie dem Quittungsfile zusammenstellt. 


Das zu verschickende Mail soll folgenden Text haben:

	Sehr geehrte:r [Herkunft-Name der Rechnung]
	
	Am [Timestamp der Quittungsdatei] wurde die erfolgreiche Bearbeitung 
	der Rechnung [Rechnungsnummer] im Zahlungssystem [Hostname-Zahlungssystem] gemeldet.
	
	In der Beilage finden Sie die Dateien in komprimierter Form.
	
	Mit freundlichen Grüssen
	[IhrVorname] [IhrNachname]


<br>
<br>

**Kontrollen**

Bauen Sie Kontrollen ein, um festzustellen, ob «die» Rechnung richtig verarbeitet wurde, bzw. ob und wann und wie ein allfälliger Abbruch stattgefunden hat. Am einfachsten sind Log-Files.
Konfiguration
Gut ist, wenn das System 'von aussen' konfiguriert / eingestellt werden kann (separates Config-File).


		

## Bewertung und Note

**Funktionalität**

Der automatische Ablauf und die korrekten Resultate bestimmen die Note.

Die Bewertung geschieht über einen Live-Test mit von der Lehrperson erstellten Test-Dateien. Es gibt eine normale ‘richtiges’ Testdatei und eine Testdatei, die unmöglich zu verarbeiten ist. Dabei wird geschaut, ob das System korrekt läuft. Die korrekt Testdatei soll richtig verarbeitet werden. Die inkorrekte Testdatei darf nicht verarbeitet werden. Dies müssen Sie nachvollziehen können.

**Dokumentation**

Es wird keine Doku verlangt und ist auch nicht notwendig, da die 
Aufgabenstellung schon so detailliert ist. (In der Praxis werden Sie 
niemals so genaue Vorgaben vorfinden, und dann wird vielleicht eine
Dokumentation verlangt werden.)


### Benotung

| Note | Bedeutung |
|------| --------- |
|   6  | Komplette Verarbeitung der Vorgaben inklusive Log-Dateien zur Rekonstruktion des Ablaufes. Alle Abläufe sind automatisiert und können per Konfigurationsdatei beeinflusst werden (FTP-Server Einstellungen). Es sind diverse Sicherheitsmechanismen eingebaut (Fehlermeldungen mit aussagekräftigem und nützlichem Inhalt).     |
|   5  | Das System funktioniert aus Sicht des Kunden, wenn alles richtig definiert war (keine Fehler in der Rechnungsdatei oder wenn die Quittung nicht vom Zahlungssystem bereitgestellt wurde). |
|   4  | Viele Teile der Vorgabe wurden erreicht, aber es fehlt noch etwas damit alles automatisch funktioniert.                  |


### Detailberechnung der Note

| Punkte |      | Bezeichnung |
| ------ | ---- | ----------- |
|    2   |      | Filedownload (.data) via FTP (aus dem [Kundenserver]/out/XX21xMustermann) |
|        | [1]  | Ein Dateidownload funktioniert |
|        | [1]  | es können auch mehrere Dateien "gleichzeitig" verarbeitet werden |
|    4   |      | Lesen der Input-Datei (.data) |    
|        | [2]  | Aufspalten der Informationen |
|        | [1]  | Erkennen falscher Informationen |
|        | [1]  | Rückweisen falscher/inkorrekter Rechnung |
|    9   |      | Erstellung der _Invoice.txt |
|        | [1]  | Richtiger Filename (gem. definierter Vorgabe) |
|        | [2]  | Korrekte Darstellung und Formatierung der Rechnung mit Einrückung und Kollonierung der Rechnungzeilen |
|        | [2]  | Richtige Berechnung der End-Summe inkl. Darstellung (2-Nummen nach dem Dez-Punkt / auch 05-er Rundung!) |
|        | [2]  | Einrücken und Darstellung des QR-Code-Abschnitts |
|        | [2]  | Richtige Berechnung und Position des Zahlungsziel-Datum (Rechnungsdatum + Zahlungsziel), -> nicht Verarbeitungsdatum!! |
|    4   |      | Erstellung der _Invoice.xml |
|        | [1]  | Richtiger Filename (gem. definierter Vorgabe) |
|        | [1]  | Rechnungsnummer eingesetzt |
|        | [1]  | Summe korrekt (ohne Punkt, mit führenden Nullen) |
|        | [1]  | Zahlungsziel & Zahlungszieldatum (Korr. Datum und Formattierung YYYYmmdd) |
|    1   |      | Fileupload (2 Files (.txt und .xml) pro Rechnung) |
|        | [1]  | via FTP (auf den zweiten Server nach[Zahlungsserver]/in/XX21xMustermann) |
|        | [-1] | Abzug 1P dafür, falls die fehlerhafte Rechnung auch noch da steht  |
|    3   |      | Zip- oder tar-File Erstellung |
|        | [2]  | Zip-/tar-File mit korrektem Inhalt und Dateinamen (2 Files) |
|        | [1]  | Fileupload via FTP (auf den [Kundenserver]/in/XX21xMustermann) |
|    3   |      | Mailing |
|        | [1]  | Mail-Versand (kommt an der richtigen Adresse "heute/jetzt" an (Mailadr im Input)) |
|        | [1]  | Mail-Text und Absender fehlerlos, den Anforderungen entsprechend |
|        | [1]  | Mail-Attachment (.zip/.tar) geschickt/vorhanden |
|    5   |      | Konfiguration und Projektdateiorganisation |
|        | [1]  | "gute" Struktur der Projektdateien, Verarbeitungsdaten nicht bei den Verarbeitungs-Skript(s) |
|        | [1]  | Log-File mit vernünftigen/aussagekräftigen Informationen, z.B. Erkennung von fehlerhafter Verarbeitung |
|        | [1]  | separate Konfigurationsdatei |
|    1   |      | Automatisierung |
|        | [1]  | Scheduler eingerichtet und funktioniert (Linux "crontab" oder Win "Aufgabenplaner") |
|**32P** |      | **Total** |
||||
| **1/2**| | Halbierung der Punkte, wenn der gleiche Code schon mal (bei einem Kollegen) gesehen wurde |
||||



