![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m122 Picto](../x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

---

## Lernziel:
* Bereit werden für die LPIC-Zertifizierung

--- 

## Grundidee
Wenn Sie die in diesem ReadMe verlinkten Unterlagen verstanden haben und Anwenden können, sind Sie vom Skills-Level her bereit für die LPIC-Zertifizierungen Linux Essentials, LPIC-1 und LPIC-2. Sie erkennen am Umfang der Unterlagen, dass dies massiv mehr ist wie Sie in diesem Modul brauchen werden. Wenn Sie aber interessiert sind an der Arbeit mit Linux und auch gerne ein zusätzliches Zertifikat erlangen möchten, dass Ihnen Ihre Kompetenzen im Bereich von Linux ausweist, dann sind die LPIC-Zertifizierungen eine im Linux-Umfeld weit verbreitete Möglichkeit, Ihre Linux-Skills nachzuweisen. Dies wird Ihnen helfen, im Linux-Admin-Bereich zukünftig eine Stelle zu finden (sofern Sie das anstreben).

Offizielle Webseite von LPIC: https://www.lpi.org

### Theorie-Unterlagen:
- [Linux Essentials](lxes-de-manual.pdf) von [Tuxcadmy](https://www.tuxcademy.org/product/lxes/)
- [LPIC-1 Verlag MIPT](./LPIC%20Unterlagen/LPIC-1_Verlag%20MIPT.pdf)
- [LPIC-1 Verlag Reinwerk](./LPIC%20Unterlagen/LPIC-1_Verlag%20Reinwerk.pdf)
- [LPIC-2 Verlag Reinwerk](./LPIC%20Unterlagen/LPIC-2_Verlag%20Reinwerk.pdf)
